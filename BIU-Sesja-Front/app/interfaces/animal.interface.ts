/**
 * Created by togi on 13.02.16.
 */

export interface Animal {
    id: number;
    name: string;
    race: string;
    age: number;
    likes: number;
}