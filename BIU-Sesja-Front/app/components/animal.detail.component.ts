/**
 * Created by togi on 31.01.16.
 */
import {Component, Input, Output, EventEmitter} from 'angular2/core';
import {AnimalDeleteComponent} from "./animal.delete.component";
import {AnimalUpdateButtonComponent} from "./animal.update.button.component";
import {Animal} from "../interfaces/animal.interface";
import {Refresh} from "../interfaces/refresh.interface";
import {btnInterface} from "../interfaces/back.button.interface";


@Component({
    selector: 'animal-detail',
    //inputs: ['animalDetail'],
    outputs: ['animalEdit'],
    directives: [AnimalDeleteComponent, AnimalUpdateButtonComponent],
    template: `
        <div>
            <div class="col-lg-6">
                <div>
                    <div>
                        <h2>Szczegóły:</h2>
                    </div>
                    <div>
                        <div>
                            <label>Imię:</label>
                            <span>{{ animalDetail.name }}</span>
                        </div>
                        <div>
                            <label>Wiek:</label>
                            <span>{{ animalDetail.age }}</span>
                        </div>
                        <div>
                            <label>Rasa:</label>
                            <span>{{ animalDetail.race }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <animal-delete [animal_id]="animalDetail.id" (deleteAnimal)="animalDeleteFn($event)"></animal-delete>
            </div>
            <div class="col-lg-6">
                <animal-update-button (updateAnimal)="animalUpdateFn($event)"></animal-update-button>
            </div>
        </div>
    `
})
export class AnimalDetailComponent {

    @Input() animalDetail: Animal;

    public animalEdit: EventEmitter<Refresh> = new EventEmitter();

    constructor(){
    }

    animalDeleteFn(data:Refresh){
        this.animalEdit.emit(data);
    }
}