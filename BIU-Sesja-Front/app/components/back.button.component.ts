/**
 * Created by togi on 12.02.16.
 */
import {Component, EventEmitter} from 'angular2/core';
import {btnInterface} from "../interfaces/back.button.interface";


@Component({
    selector: 'back-button',
    inputs: [],
    outputs: ['btnAction'],
    template: `
        <div>
            <button type="button" (click)="action()">Wróć</button>
        </div>
    `
})
export class BackButtonComponent {
    public btnAction: EventEmitter<btnInterface> = new EventEmitter();

    constructor(){

    }

    action(){
        var data: btnInterface = {
            clean: true
        };

        this.btnAction.emit(data);
    }
}
