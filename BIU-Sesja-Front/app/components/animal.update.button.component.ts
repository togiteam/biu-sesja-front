/**
 * Created by togi on 12.02.16.
 */
import {Component, EventEmitter} from 'angular2/core';
import {btnInterface} from "../interfaces/back.button.interface";


@Component({
    selector: 'animal-update-button',
    inputs: [''],
    outputs: ['updateAnimal'],
    template: `
        <div>
            <button type="button" (click)="action()">Modyfikuj</button>
        </div>
    `
})
export class AnimalUpdateButtonComponent {
    public updateAnimal: EventEmitter<btnInterface> = new EventEmitter();

    constructor(){
    }

    action(){
        var data: btnInterface = {
            clean: true
        }
        this.updateAnimal.emit(data);
    }
}