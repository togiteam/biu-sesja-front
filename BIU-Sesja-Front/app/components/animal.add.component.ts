/**
 * Created by togi on 10.02.16.
 */
import {Component, Input, EventEmitter, Output} from 'angular2/core';
import {AnimalService} from '../services/animal.service';
import {FORM_DIRECTIVES, ControlGroup, AbstractControl, FormBuilder, Validators, Control} from "angular2/common";
import {Animal} from "../interfaces/animal.interface";
import {Refresh} from "../interfaces/refresh.interface";

@Component({
    selector: 'animal-add',
    directives: [FORM_DIRECTIVES],
    inputs: [],
    outputs: ['refresh'],
    template: `
        <div *ngIf="!is_success">
            <div>
                <h2>Dodaj nowe zwierzę do Zoo</h2>
            </div>
            <div>
                <form [ngFormModel]="addForm" (ngSubmit)="addAnimalOnSubmit(addForm.value)">
                    <div>
                        <label>Imię:</label>
                        <input type="text" placeholder="Podaj imię zwierzęcia" [ngFormControl]="name">
                        <div *ngIf="name.hasError('required')">Wartość wymagana</div>
                    </div>
                    <div>
                        <label>Rasa:</label>
                        <input type="text" placeholder="Podaj rasę zwierzęcia" [ngFormControl]="race">
                        <div *ngIf="race.hasError('required')">Wartość wymagana</div>
                    </div>
                    <div>
                        <label>Wiek:</label>
                        <input type="number" placeholder="Podaj wiek zwierzęcia" [ngFormControl]="age">
                        <div *ngIf="age.hasError('NegativeValue')">Wiek nie może być ujemny</div>
                    </div>
                    <div>
                        <label>Polubienia:</label>
                        <input type="number" placeholder="Podaj polubienia zwierzęcia" [ngFormControl]="likes">
                        <div *ngIf="likes.hasError('NegativeValue')">Polubienia nie mogą być ujemne</div>
                    </div>
                    <button type="submit" [disabled]="!addForm.valid">Dodaj</button>
                </form>
            </div>
        </div>
        <div *ngIf="is_success">
            <div>
                <span>{{message}}</span>
            </div>
            <div>
                <button type="button" (click)="refreshList()">Super</button>
            </div>
        </div>
    `
})
export class AnimalAddComponent {

    is_success: boolean = false;
    message: string = "";

    public refresh: EventEmitter<Refresh> = new EventEmitter();

    refreshList(){
        var data: Refresh = {
            reload: true
        };

        this.is_success = false;
        this.refresh.emit(data);

    };

    addForm: ControlGroup;

    name: AbstractControl;
    race: AbstractControl;
    age: AbstractControl;
    likes: AbstractControl;

    constructor(fb: FormBuilder, private as: AnimalService){
        this.addForm = fb.group({
            'name': ['', Validators.required],
            'race': ['', Validators.required],
            'age': [0, this.positiveValidator],
            'likes': [0, this.positiveValidator]
        });

        this.name = this.addForm.controls['name'];
        this.race = this.addForm.controls['race'];
        this.age = this.addForm.controls['age'];
        this.likes = this.addForm.controls['likes'];
    }

    addAnimalOnSubmit(data:Animal){
        var _json = JSON.stringify(data);
        return this.as.createAnimal(_json).subscribe(
            response => {
                if(response.status == "success") this.is_success = true;
                if(response.data && response.data.message) this.message = response.data.message;
            }
        );
    }

    positiveValidator(control: Control){
        if(control.value < 0){
            return {'NegativeValue': true}
        }
    }
}
