/**
 * Created by togi on 12.02.16.
 */
import {Component, EventEmitter} from 'angular2/core';
import {AnimalService} from "../services/animal.service";
import {Refresh} from "../interfaces/refresh.interface";


@Component({
    selector: 'animal-delete',
    inputs: ['animal_id'],
    outputs: ['deleteAnimal'],
    template: `
        <div>
            <button type="button" (click)="action()">Usuń</button>
        </div>
    `
})
export class AnimalDeleteComponent {
    public animal_id: number;
    public deleteAnimal: EventEmitter<Refresh> = new EventEmitter();

    constructor(private as: AnimalService){

    }

    action(){
        return this.as.deleteAnimal(this.animal_id).subscribe(
            response => {

                if(response.status == "success") {
                    var data: Refresh = {
                        reload: true
                    };

                    this.deleteAnimal.emit(data);
                }
            }
        );
    }
}