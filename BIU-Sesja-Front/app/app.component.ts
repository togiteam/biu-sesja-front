import {Component} from 'angular2/core';
import {AnimalService} from "./services/animal.service";
import {AnimalDetailComponent} from "./components/animal.detail.component";
import {AnimalAddComponent} from "./components/animal.add.component";
import {HTTP_PROVIDERS} from "angular2/http";
import {BackButtonComponent} from "./components/back.button.component";
import {Animal} from "./interfaces/animal.interface";
import {Refresh} from "./interfaces/refresh.interface";
import {btnInterface} from "./interfaces/back.button.interface";


@Component({
    providers: [AnimalService, HTTP_PROVIDERS],
    selector: 'my-app',
    directives: [AnimalDetailComponent, AnimalAddComponent, BackButtonComponent],
    template: `
        <div class="container">
            <div>
                <h2>Stan zwierząt w ZOO:</h2>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <ul>
                        <li *ngFor="#animal of animals" (click)="onSelect(animal)">{{animal.name}} - {{animal.likes}}</li>
                    </ul>
                </div>
                <div class="col-lg-8" *ngIf="showDetails">
                    <div>
                        <back-button (btnAction)="cleanSelected($event)"></back-button>
                    </div>
                    <div>
                        <animal-detail [animalDetail]="selectedAnimal" (animalEdit)="reloadList($event)"></animal-detail>
                    </div>
                </div>
                <div class="col-lg-8" *ngIf="!showDetails">
                    <animal-add (refresh)="reloadList($event)"></animal-add>
                </div>
            </div>
        </div>
    `
})
export class AppComponent {
    public animals: Animal[];
    public selectedAnimal:Animal;
    public showDetails:boolean = false;

    constructor(private as: AnimalService){
    }

    ngOnInit(){
        this.as.getAnimals().subscribe(
            data => {
                this.animals = data.data.animals;
            }
        );
    }

    onSelect(animal:Animal){
        this.selectedAnimal = animal;
        this.showDetails = true;
    }

    reloadList(data:Refresh){
        console.log('some event in app component');
        this.showDetails = false;
        this.ngOnInit();
    }

    cleanSelected(data:btnInterface){
        this.showDetails = false;
    }
}