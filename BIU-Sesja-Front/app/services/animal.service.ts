/**
 * Created by togi on 31.01.16.
 */
import {Injectable} from 'angular2/core';
import {HTTP_PROVIDERS, Http, Headers, Response} from 'angular2/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';


@Injectable()
export class AnimalService {
    private HOST: string = "http://127.0.0.1:8080";

    constructor(private _http: Http){
    }

    getAnimals(){
        return this._http.get(this.HOST + "/api/animal/all/", {
            headers: new Headers({
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE',
                'Access-Control-Allow-Headers': 'accept, cache-control, origin, ' +
                                                'x-requested-with, x-file-name, content-type',
                //'Content-Type': 'application/json'
            })
        }).map((res:Response) => res.json());
    }

    createAnimal(animal_json){
        return this._http.post(this.HOST + "/api/animal/animal/", animal_json)
            .map((res:Response) => res.json());
    }

    updateAnimal(animal_json, animal_id){
        return this._http.put(this.HOST + "/api/animal/animal/" + animal_id, animal_json)
            .map((res:Response) => res.json());
    }

    deleteAnimal(animal_id){
        return this._http.delete(this.HOST + "/api/animal/delete/" + animal_id + "/")
            .map((res:Response) => res.json());
    }
}