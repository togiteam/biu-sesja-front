# 1. Uruchomienie projektu

## 1.1. Instalacja zależności

```npm install```

## 1.2. Uruchomienie projektu

```npm start```

## 1.3 Start chromium with disabled CORS

chromium-browser --disable-web-security